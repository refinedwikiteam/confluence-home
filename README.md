# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a home for Confluence used for our testing. It's the new test data.

### How do I get set up? ###

The repo consists of a Confluence home directory with a H2 database. Simply point your standalone server towards this directory and you should be good to go.

### Contribution guidelines ###

Don't forget to remove any plugins from the database before committing. To do this, download and unzip [the h2 database](http://www.h2database.com/html/download.html) driver and use the following command:

    java -cp h2*.jar org.h2.tools.Shell

These are the settings you should use

    URL       jdbc:h2:[confluence-home location]/database/h2db
    Driver    org.h2.Driver
    User      sa
    Password  [empty]

Once connected run the following command:

    truncate table PLUGINDATA;

### Who do I talk to? ###

Richard
